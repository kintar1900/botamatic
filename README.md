# Botamatic
An opinionated Discord bot framework for Go.

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/kintar1900/botamatic.svg)](https://pkg.go.dev/gitlab.com/kintar1900/botamatic)

![current release](https://img.shields.io/badge/dynamic/json?color=blue&label=current%20release&query=%24.latestRelease&url=https%3A%2F%2Fgitlab.com%2Fkintar1900%2Fbotamatic%2F-%2Fjobs%2Fartifacts%2Fmain%2Fraw%2Fbadge-info.json%3Fjob%3DUpdateBadges&style=flat-square&logo=gitlab) ![GitLab pipeline](https://img.shields.io/gitlab/pipeline/kintar1900/botamatic/main.svg?style=flat-square&logo=gitlab) ![coverage](https://img.shields.io/gitlab/coverage/kintar1900/botamatic/main?logo=gitlab&style=flat-square)

# Overview
