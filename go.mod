module gitlab.com/kintar1900/botamatic

go 1.15

require (
	github.com/bwmarrin/discordgo v0.22.0
	github.com/jstemmer/go-junit-report v0.9.1 // indirect
	github.com/stretchr/testify v1.6.1
)
