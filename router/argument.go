package router

import (
    "strconv"
    "strings"
)

// Argument represents a parsed argument from a command
type Argument string

// String() returns the original string value of the argument
func (a Argument) String() string {
    return string(a)
}

// AsInt() converts the argument to an integer, returning false in the second value if the conversion fails
func (a Argument) AsInt() (int64, bool) {
    if v, err := strconv.ParseInt(a.String(), 10, 0); err == nil {
        return v, true
    }
    return 0, false
}

// AsFloat() converts the argument to a float, returning false in the second value if the conversion fails
func (a Argument) AsFloat() (float64, bool) {
    if v, err := strconv.ParseFloat(a.String(), 0); err == nil {
        return v, true
    }
    return 0, false
}

// AsBool() converts the argument to a bool, returning false in the second value if the conversion fails
func (a Argument) AsBool() (bool, bool) {
    if v, err := strconv.ParseBool(a.String()); err == nil {
        return v, true
    }
    return false, false
}

type ArgumentList []Argument

// Pop removes and returns the first argument from the argument list
func (args *ArgumentList) Pop() Argument {
    localArgs := *args
    arg := localArgs[0]
    *args = localArgs[1:]
    return arg
}

// String joins the string values of the arguments in the list together with spaces
func (args ArgumentList) String() string {
    strs := make([]string, len(args))
    for i, a := range args {
        strs[i] = a.String()
    }
    return strings.Join(strs, " ")
}
