package router

import (
    "fmt"
    "testing"
)

func TestArgumentList_Pop(t *testing.T) {
    list := ArgumentList{
        "this",
        "is",
        "a",
        "list",
        "of",
        "arguments",
    }

    v := list.Pop()
    if v != "this" {
        t.Error(fmt.Sprintf("Pop() returned incorrect result '%s' as the first argumentt", v))
    }
    if len(list) != 5 {
        t.Error(fmt.Sprintf("after Pop(), expected len(list) == 5, but got %d", len(list)))
    }
}
