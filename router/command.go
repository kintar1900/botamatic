package router

import (
    "github.com/bwmarrin/discordgo"
)

// A Command represents functionality of a bot.  It must have a Name and may have zero or more Aliases, all of which are a word or phrase that triggers
// the associated Handler.
//
// Commands can be a single word, like `help` or `status`, or multi-word phrases, like `current uptime` or `current memory`, and may contain wildcards.
// When matching commands to incoming messages, the router tokenizes the command phrases and the incoming text by splitting on whitespace.  The command
// with the most matching tokens is the one that is executed.  If multiple commands have the same number of tokens, exact matches are selected in favor
// of wildcard matches, and remaining ambiguity is resolved in lexical order of the non-wildcard tokens.
//
// Do NOT prepend a trigger or escape character, such as '!', to the name or aliases! This is handled by the Router itself.
//
// Wildcards are allowed to match multiple input tokens, unless the input Token would match the following command Token. In other words, the command
//  say * to bob
// would match the following:
//  say you're awesome to bob
//  say don't ever change to bob
// But not:
//  say want to go to fred's? to bob
//
// Input tokens matched to a wildcard are included in order of appearance at the beginning of the ExecutionContext's arguments.
//
// You can define commands that take multiple options in several ways.  First, by using two commands with the same names and/or aliases, such as the uptime and
// memory examples above.  Second, by specifying only the `current` command, and providing logic to handle the first arguments of `uptime` and `memory`.
// Third, by defining the `current` command, and specifying two subcommands named `uptime` and `memory`, each with their own handler functions.
type Command struct {
    Name    string
    Handler ExecutionHandler
    Aliases []string
    // Tells the router the minimum number of arguments must be supplied to this command in order for it to function as intended.
    // This is used to break ties in command routing.
    MinimumArgumentCount int
    // Optional description of the command for built-in help functionality
    Description string
    // Optional example usage for built-in help functionality
    Usage string
    SubCommands []Command
}

// ExecutionHandler is a function responsible for performing logic in response to a Command
type ExecutionHandler func(ctx *ExecutionContext)

// ExecutionContext represents the state of the bot at the time of command execution, as well as providing access to the discordgo.Session,
// original discordgo.Message, any data injected by the middleware, and helper functions to quickly respond with Text or Embed replies.
type ExecutionContext struct {
    // The Name of the command, or the Alias that triggered ti
    Trigger string
    // The remaining text of the original message after removing the trigger, split on whitespace
    Arguments ArgumentList

    Message *discordgo.Message
    Session *discordgo.Session

    contextData map[string]interface{}
}

// Get returns the value of the given key, and a bool indicating the presence of the key
func (c ExecutionContext) Get(key string) (interface{}, bool) {
    v, ok := c.contextData[key]
    return v, ok
}

// Set associates the given key with the given interface in the current ExecutionContext
func (c *ExecutionContext) Set(key string, value interface{}) {
    c.contextData[key] = value
}

// Reply sends the specified string to the channel on which this command was received
func (c *ExecutionContext) Reply(text string) error {
    _, err := c.Session.ChannelMessageSend(c.Message.ChannelID, text)
    return err
}

// ReplyEmbed sends the specified Embed to the channel on which this command was received
func (c *ExecutionContext) ReplyEmbed(embed *discordgo.MessageEmbed) error {
    _, err := c.Session.ChannelMessageSendEmbed(c.Message.ChannelID, embed)
    return err
}
