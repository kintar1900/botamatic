package router

import (
    "fmt"
    "github.com/bwmarrin/discordgo"
    "log"
    "sort"
    "strings"
)

type commandRoute struct {
    tokens      TokenList
    command     Command
    tokenWeight int
    lexicalSort string
}

func tokenizeAll(cmd []Command) []commandRoute {
    r := make([]commandRoute, 0)
    for _, c := range cmd {
        r = append(r, tokenize(c)...)
    }
    return r
}

func tokenize(cmd Command, previousTokens ...Token) []commandRoute {
    routes := make([]commandRoute, 0)
    tokens := TokenList(append(previousTokens, Tokenize(cmd.Name)...))

    routes = append(routes, commandRoute{
        tokens:      tokens,
        command:     cmd,
        tokenWeight: Weight(tokens),
        lexicalSort: tokens.String(),
    })

    for _, alias := range cmd.Aliases {
        tokens = append(previousTokens, Tokenize(alias)...)
        routes = append(routes, commandRoute{
            tokens:      tokens,
            command:     cmd,
            tokenWeight: Weight(tokens),
            lexicalSort: tokens.String(),
        })
    }

    subRoutes := make([]commandRoute, 0)
    for _, sub := range cmd.SubCommands {
        for _, r := range routes {
            subRoutes = append(subRoutes, tokenize(sub, r.tokens...)...)
        }
    }

    return append(routes, subRoutes...)
}

type commandRouteSorter []commandRoute

func (c commandRouteSorter) Len() int {
    return len(c)
}

func (c commandRouteSorter) Less(i, j int) bool {
    tokensDiff := c[i].tokenWeight - c[j].tokenWeight
    if tokensDiff == 0 {
        return c[i].lexicalSort < c[j].lexicalSort
    }

    return tokensDiff < 0
}

func (c commandRouteSorter) Swap(i, j int) {
    c[i], c[j] = c[j], c[i]
}

type ChainedExecutionHandler func(ctx *ExecutionContext) (proceed bool)

type Router struct {
    triggers []string
    routes   []commandRoute
    middleware []ChainedExecutionHandler
}

func New(triggers []string, commands []Command, middleware []ChainedExecutionHandler) Router {
    routes := tokenizeAll(commands)
    sort.Sort(commandRouteSorter(routes))
    triggersCopy := make([]string, len(triggers))
    for i := range triggers {
        triggersCopy[i] = strings.ToLower(triggers[i])
    }
    return Router{
        triggers: triggers,
        routes:   routes,
        middleware: middleware,
    }
}

// Start adds this router's handler functions to the session
func (r Router) Start(session *discordgo.Session) {
    session.AddHandler(r.messageCreateHandler)
}

func (r Router) doTrigger(input string) (string, TokenList, bool) {
    inputCopy := strings.ToLower(input)
    for _, t := range r.triggers {
        if strings.Index(inputCopy, t) == 0 {
            return input[:len(t)], Tokenize(input[len(t):]), true
        }
    }

    return "", nil, false
}

type weightedMatch struct {
    r commandRoute
    m TokenListMatch
}

func (i weightedMatch) Less(j weightedMatch) bool {
    return i.m.MatchLength < j.m.MatchLength ||
        (i.m.MatchLength == j.m.MatchLength && i.m.WildsCount > j.m.WildsCount)
}

func (r Router) messageCreateHandler(s *discordgo.Session, m *discordgo.MessageCreate) {
    trigger, input, triggered := r.doTrigger(m.Message.Content)
    if !triggered {
        return
    }

    // I apparently can't help but optimize early. :P
    matches := make([]weightedMatch, len(r.routes))
    for routeNum, route := range r.routes {
        matches[routeNum].r = route
        matches[routeNum].m = route.tokens.Match(input)
    }

    sort.Slice(matches, func(i, j int) bool {
        return matches[i].Less(matches[j])
    })

    result := matches[len(matches)-1]

    if result.m.MatchLength == 0 {
        return
    }

    ctx := &ExecutionContext{
        Trigger:     trigger,
        Arguments:   result.m.Arguments,
        Message:     m.Message,
        Session:     s,
        contextData: make(map[string]interface{}),
    }

    if len(matches) > 1 {
        match2 := matches[len(matches)-2]
        if !match2.Less(result) {
            log.Printf("two closest matches are identical\n\tinput '%s'\n\tcommand1 '%v'\n\tcommand2: '%v'", input, result.m.MatchLength,
                match2.m.MatchLength)
            _ = ctx.Reply(fmt.Sprintf("Two comamnds match that input.  %s and %s.  Please talk to my programmer about that.",
                result.r.command.Name, match2.r.command.Name))
            return
        }
    }

    for _, m := range r.middleware {
        if !m(ctx) {
           return
        }
    }

    result.r.command.Handler(ctx)
}
