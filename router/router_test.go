package router

import (
    "fmt"
    "github.com/bwmarrin/discordgo"
    "github.com/stretchr/testify/assert"
    "testing"
)

func Test_tokenize(t *testing.T) {
    responseMap := make(map[int]int)

    hf := func(funcNum int) func(*ExecutionContext) {
        return func(_ *ExecutionContext) {

            responseMap[funcNum]++
        }
    }

    commands := makeCommands(hf)

    routes := tokenizeAll(commands)
    lexicalStrings := make([]string, 0)
    for _, r := range routes {
        r.command.Handler(nil)
        lexicalStrings = append(lexicalStrings, r.lexicalSort)
    }

    assert.Equal(t, 1, responseMap[1])
    assert.Equal(t, 3, responseMap[2])
    assert.Equal(t, 6, responseMap[3])
    assert.Equal(t, 6, responseMap[4])

    expectedLexicalStrings := []string{
        "help",
        "subcommander",
        "sc",
        "subc",
        "subcommander sc1",
        "sc sc1",
        "subc sc1",
        "subcommander sc2",
        "sc sc2",
        "subc sc2",
        "subcommander 1",
        "sc 1",
        "subc 1",
        "subcommander 2",
        "sc 2",
        "subc 2",
    }

    for _, es := range expectedLexicalStrings {
        assert.Contains(t, lexicalStrings, es)
    }
}

func makeCommands(hf func(funcNum int) func(*ExecutionContext)) []Command {
    return []Command{
        {
            Name:        "help",
            Handler:     hf(1),
            Aliases:     nil,
            Description: "A help command",
            Usage:       "",
            SubCommands: nil,
        },
        {
            Name:        "subcommander",
            Handler:     hf(2),
            Aliases:     []string{"sc", "subc"},
            Description: "",
            Usage:       "",
            SubCommands: []Command{
                {
                    Name:        "sc1",
                    Handler:     hf(3),
                    Aliases:     []string{"1"},
                    Description: "",
                    Usage:       "",
                    SubCommands: nil,
                },
                {
                    Name:        "sc2",
                    Handler:     hf(4),
                    Aliases:     []string{"2"},
                    Description: "",
                    Usage:       "",
                    SubCommands: nil,
                },
            },
        },
    }
}

func TestRouter_route(t *testing.T) {
    defer func() {
        err := recover()
        if err != nil {
            t.Log("execution panicked:", err)
            t.Fail()
        }
    }()

    responseMap := make(map[int]int)

    hf := func(funcNum int) func(*ExecutionContext) {
        return func(_ *ExecutionContext) {

            responseMap[funcNum]++
        }
    }

    commands := makeCommands(hf)
    router := New([]string{"!"}, commands, nil)

    router.messageCreateHandler(nil, &discordgo.MessageCreate{
       Message: &discordgo.Message{
           Content:          "!help",
       },
    })
    router.messageCreateHandler(nil, &discordgo.MessageCreate{
       Message: &discordgo.Message{
           Content:          "!sc sc2 foo bar",
       },
    })

    for k, v := range responseMap {
        fmt.Printf("%d: %d\n", k, v)
    }

    assert.Equal(t, 1, responseMap[1])
    assert.Equal(t, 1, responseMap[4])
}

func Test_MiddlewarePass(t *testing.T) {
    var cmdInvoked, mwInvoked bool
    commands := []Command {
        {
            Name:                 "noop",
            Handler: func(ctx *ExecutionContext) {
                cmdInvoked = true
            },
            Aliases:              nil,
            MinimumArgumentCount: 0,
            Description:          "does nothing",
            Usage:                "noop",
            SubCommands:          nil,
        },
    }
    middleware := []ChainedExecutionHandler {
        func(ctx *ExecutionContext) (proceed bool) {
            mwInvoked = true
            proceed = true
            return
        },
    }
    r := New([]string {"!"}, commands, middleware)

    r.messageCreateHandler(nil, &discordgo.MessageCreate{
        Message: &discordgo.Message{
            Content: "!noop",
        },
    })

    assert.True(t, mwInvoked, "middleware was not invoked")
    assert.True(t, cmdInvoked, "command was not invoked")
}

func Test_MiddlewareAbort(t *testing.T) {
    var cmdInvoked, mwInvoked bool
    commands := []Command {
        {
            Name:                 "noop",
            Handler: func(ctx *ExecutionContext) {
                cmdInvoked = true
            },
            Aliases:              nil,
            MinimumArgumentCount: 0,
            Description:          "does nothing",
            Usage:                "noop",
            SubCommands:          nil,
        },
    }
    middleware := []ChainedExecutionHandler {
        func(ctx *ExecutionContext) (proceed bool) {
            mwInvoked = true
            proceed = false
            return
        },
    }
    r := New([]string {"!"}, commands, middleware)

    r.messageCreateHandler(nil, &discordgo.MessageCreate{
        Message: &discordgo.Message{
            Content: "!noop",
        },
    })

    assert.True(t, mwInvoked, "middleware was not invoked")
    assert.False(t, cmdInvoked, "middleware did not abort command execution")
}
