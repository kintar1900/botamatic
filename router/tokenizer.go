package router

import (
    "regexp"
    "strings"
)

var space = regexp.MustCompile(`\s+`)

type Token string

// #####
// Token members

func (t Token) String() string {
    return string(t)
}

// IsWildcard returns true if this token is a wildcard
func (t Token) IsWildcard() bool {
    return "*" == t.String()
}

// Matches returns true if the two tokens are a match, or if the first token is a wildcard
func (t Token) Matches(t2 Token) bool {
    return t.IsWildcard() || strings.EqualFold(t.String(), t2.String())
}

// TokenList represents an ordered list of tokens used for matching
type TokenList []Token

// ExactlyMatches returns true if two TokenList instances are the same length and all elements satisfy
// strings.EqualFold(list1[index].String(), list2[index].String())
func (tl TokenList) ExactlyMatches(tl2 TokenList) bool {
    if len(tl) != len(tl2) {
        return false
    }

    for i := range tl {
        if !strings.EqualFold(tl[i].String(), tl2[i].String()) {
            return false
        }
    }

    return true
}

// Weight determines the ordering weight of a TokenList, based on the number of tokens, less the number of wildcard tokens
func Weight(tl TokenList) int {
    weight := 0
    for _, t := range tl {
        if !t.IsWildcard() {
            weight++
        }
    }
    return weight
}

func (tl TokenList) String() string {
    strs := make([]string, len(tl))
    for i, e := range tl {
        strs[i] = e.String()
    }
    return strings.Join(strs, " ")
}

// Tokenize turns a string into a TokenList by collapsing all whitespace into single spaces and splitting the result on space
func Tokenize(s string) TokenList {
    s = space.ReplaceAllString(s, " ")
    s = strings.TrimSpace(s)
    split := strings.Split(s, " ")
    tokens := make([]Token, len(split))
    for i, t := range split {
        tokens[i] = Token(t)
    }
    return tokens
}

// TokenListMatch represents the results of matching a TokenList to a string
type TokenListMatch struct {
    // Input holds the original string that was matched
    Input string
    // MatchLength is the number of tokens in the list that matched
    MatchLength int
    // WildsCount is the number of tokens in the match that were wildcards
    WildsCount int
    // Arguments is an ArgumentList constructed from any matched wildcards and leftover tokens
    Arguments ArgumentList
}

// Match builds a TokenListMatch from a TokenList and a string
func (tl TokenList) Match(inputTokens TokenList) TokenListMatch {
    tlm := TokenListMatch{}
    matched := true
    for idx := 0; matched && idx < len(inputTokens) && idx < len(tl); idx++ {
        matched = tl[idx].Matches(inputTokens[idx])
        if !matched {
            break
        }

        tlm.MatchLength++

        if tl[idx].IsWildcard() {
            tlm.WildsCount++
            tlm.Arguments = append(tlm.Arguments, Argument(inputTokens[idx].String()))
        }
    }

    // If we matched at least one token, build an argument list from everything that didn't match
    if tlm.MatchLength > 0 && tlm.MatchLength < len(inputTokens) {
        remainder := inputTokens[tlm.MatchLength:]
        args := make([]Argument, len(remainder))
        for i, r := range remainder {
            args[i] = Argument(r.String())
        }
        tlm.Arguments = append(tlm.Arguments, args...)
    }

    return tlm
}

// ######
// TokenList sort types

type tokenListByLength []TokenList

func (s tokenListByLength) Len() int {
    return len(s)
}

func (s tokenListByLength) Less(i, j int) bool {
    return len(s[i]) < len(s[j])
}

func (s tokenListByLength) Swap(i, j int) {
    s[j], s[i] = s[i], s[j]
}
