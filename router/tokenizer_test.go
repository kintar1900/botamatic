package router

import (
    "github.com/stretchr/testify/assert"
    "testing"
)

func TestToken_Matches_Wildcard(t *testing.T) {
    wc := Token("*")
    input := Token("not a wildcard")

    if input.Matches(wc) {
        t.Error("non-wildcard token matched a wildcard input")
    }

    if !wc.Matches(input) {
        t.Error("wildcard token failed to match input")
    }
}

func TestTokenList_Match(t *testing.T) {
    expectedMatchLength := 4
    expectedWildcardCount := 1
    expectedArgs := ArgumentList{
        "three",
        "five",
    }

    commandTokens := Tokenize("one two * four")
    match := commandTokens.Match(Tokenize("one two three four five"))

    assert.True(t, match.MatchLength != 0, "match failed")
    assert.Equal(t, expectedMatchLength, match.MatchLength, "match did not meet expected length")
    assert.Equal(t, expectedWildcardCount, match.WildsCount, "wildcard count was not correct")
    assert.Equal(t, expectedArgs, match.Arguments, "arguments list was incorrect")
}

func TestTokenList_NoMatch(t *testing.T) {
    expectedMatchLength := 3
    expectedWildcardCount := 1
    expectedArgs := ArgumentList{
        "three",
        "five",
    }

    commandTokens := Tokenize("one two * four")
    match := commandTokens.Match(Tokenize("one two three five"))

    assert.True(t, match.MatchLength != 0, "match failed")
    assert.Equal(t, expectedMatchLength, match.MatchLength, "match did not meet expected length")
    assert.Equal(t, expectedWildcardCount, match.WildsCount, "wildcard count was not correct")
    assert.Equal(t, expectedArgs, match.Arguments, "arguments list was incorrect")
}
