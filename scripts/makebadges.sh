#!/usr/bin/env bash

LATEST_RELEASE=$(git describe --tags $(git rev-list --tags --max-count=1))

echo "{\"latestRelease\": \"$LATEST_RELEASE\" }" > badge-info.json
