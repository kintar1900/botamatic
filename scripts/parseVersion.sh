#!/usr/bin/env bash

set -e

# Calculate current tree version
version="$(git describe --tags --dirty)"
echo "$version"
